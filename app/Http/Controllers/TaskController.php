<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use Validator;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Task::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()]);
        }

        Task::create($request->all());

        return response("Tarefa criada com sucesso", 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::find($id);

        if($task) {
            return $task;
        }

        return response('Tarefa não existente!', 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()]);
        }

        $task = Task::find($id);

        if($task) {
            $task->update($request->all());

            return response("Tarefa modificada com sucesso", 200);
        }

        return response('Tarefa não existente!', 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = task::find($id);
        
        if($task) {
            $task->delete();

            return response("Tarefa eliminada com sucesso", 200);
        }

        return response('Tarefa não existente!', 404);
    }

    public function archive($id) 
    {
        $task = task::find($id);

        if($task) {
            $task->archived = true;

            $task->save();
            return response("Tarefa arquivada com sucesso", 200);
        }

        return response('Tarefa não existente!', 404);
    }

    public function unarchive($id) 
    {
        $task = task::find($id);

        if($task) {
            $task->archived = false;

            $task->save();
            return response("Tarefa desarquivada com sucesso", 200);
        }

        return response('Tarefa não existente!', 404);
    }
}
