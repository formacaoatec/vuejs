<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\Task::class, function (Faker $faker) {
    return [
        'title' => $faker->words(3, true),
        'finished' => $faker->boolean,
        'due_date' => $faker->dateTimeBetween('+1 days', '+3 months'),
    ];
});
