# Exercício de avaliação de VueJS

## Instalação da API

### Clone do repoditório
```
git clone https://bitbucket.org/formacaoatec/vuejs.git
```

### Instalar dependências
```
composer install
```

### Criação da base de dados e configuração
Após criar a base de dados, configure o ficheiro .env

### Executar migrations
```
php artisan migrate --seed
```

### Arrancar servidor
```
php artisan serve
```
## Uso da API

### Obter todos as tarefas
```
GET /api/tasks
```

### Obter uma tarefa específico
```
GET /api/tasks/{id}
```

### Criar uma tarefa
```
POST /api/tasks
{
    "title": "Ver um filme",
    "completed": false,
    "due_date": "2017-11-08",
    "archived": true,
}
```

### Modificar uma tarefa
```
PUT /api/tasks/{id}
{
    "title": "Ver um filme",
    "completed": false,
    "due_date": "2017-11-08",
    "archived": false
}
```

### Arquivar uma tarefa
```
PUT /api/tasks/{id}/archive
```

### Modificar uma tarefa
```
PUT /api/tasks/{id}/unarchive
```

### Eliminar uma tarefa
```
DELETE /api/tasks/{id}
```
